todo-list-seb
=============

## User Story
As a user of the TODO application, I would like to select an existing user and view that user's TODO list.
Then I can manipulate the user's todo list.

## Requirements

- Git
- Java 8

## Running the Application

This application is built using [Gradle](http://www.gradle.org/) and [Spring Boot](http://docs.spring.io/spring-boot/docs/1.2.0.RC2/reference/htmlsingle/)
to bootstrap the Spring Framework. There are several ways you can run the application:

1. Use the Gradle Wrapper from the project directory: `./gradlew run`
1. Simply run the main method in the `Application` class from your IDE
1. Spring Boot also builds an executable war. To build and execute the war run `./gradlew && java -jar build/libs/todo-app-1.0.0-SNAPSHOT.war`

The application will be running on [http://localhost:8080](http://localhost:8080)

## Links

- [Git Docs](http://git-scm.com/doc)
- [Gradle Docs](http://www.gradle.org/documentation)
- [Spring Framework Reference](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/)
- [Spring Boot Reference](http://docs.spring.io/spring-boot/docs/1.2.0.RC2/reference/htmlsingle/)
- [JSONPlaceholder Docs](http://jsonplaceholder.typicode.com/)
- [jQuery API Docs](http://api.jquery.com/)
- [Bootstrap Docs](http://getbootstrap.com/css/)
- [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Notes

a few notes that I have regarding the coding experience:

- I implemented the backend code as well as the front end code.
- The way the application works is when the server starts it reads all the data from JsonPlaceholder and loads the user and task data to the server's memory, so JsonPlaceholder's data is read only once when the server starts so the server will have some data.
- From then on, the client reads and updates the data from the server's data.
- The client can choose its user, and the list of tasks for that user will be loaded.
- The client can add or check off any tasks they want.
- The client can choose to trim the completed tasks all the way to the server.

Back end:

- I used all the libraries available and implemented a rest service.
- I probably did a little bit more than asked for the REST api. I implemented all CRUD functions for todo tasks.
- I dont have that much experience with spring boot so I couldnt add some unit tests, I used soapui to test the rest service instead
- I wanted to test from a client from a different server origin, so i added CORS to the requests, that should be taken out once out of development.

Jquery and Angular Front end:

- There are two different front end applications ready
- The angular front end can be accessed on the url: http://localhost:8080/angular/index.html
- The Jquery front end is now in place at: http://localhost:8080/jquery/index.html
- Both front end apps can be accessed via http://localhost:8080
- The web app is responsive, I wanted to used bootstrap glyphicons but they werent available.
- The way the app works is the user chooses the select option for the user and the user tasks will be loaded.
- A task can get added, deleted and check updated all the way to the server.
- When a task gets added a new random id based on the time is provided and the app knows the current user id.
- The archiving button is still offered to trim all the done tasks.

## Running

- gradle assemble or ./gradlew
- java -jar build/libs/todo-list-seb-1.0.0-SNAPSHOT.war
