var TodoApp = {

  init: function() {
    this.userId = 0;
    this.selectUser = $("#todo-user-select");
    this.todoList = $("#todo-list");
    this.todoInput = $('#todo-input-text');
    this.todoControl = $('#todo-control');
    this.todoArchiveButton = $('#todo-archive-btn');
    this.selectUser.on('change', this.getCurrentUser);
    this.todoInput.on('change', this.addTodo);   
    this.todoList.on('change','.check-todo',this.checkComplete);
    this.todoList.on('click','.todo-remove', this.removeItem);
    this.todoList.on('click','#todo-check-all', this.checkAll);
    this.todoArchiveButton.on('click', this.archiving);
    TodoApp.getUsers();
  },

  ////

  getUsers: function() {
    console.log("getUsers");
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/user/",
      //data: data,
      success: function(data) {
        //console.log(data);
        var dataLength = data.length;
        for (var i = 0; i < dataLength; i++) {
          $('#todo-user-select').append('<option value='+data[i].id+'>'+data[i].username+'</option>');
        }
      }
    });
  },

  getCurrentUser: function() {
    TodoApp.userId = TodoApp.selectUser.val();
    console.log("getCurrentUser "+TodoApp.userId);
    TodoApp.getTodos();
  },

  getTodos: function() {
    console.log("getTodos");

    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/todo/user/"+TodoApp.userId,
      //data: data,
      success: function(data) {
        //console.log(data);
        TodoApp.removeAll();
        TodoApp.fromObject(data);
      }
    });
  },

  addTodo: function() {
    console.log("addTodo");
    var value = TodoApp.todoInput.val();
    var randomId = TodoApp.getRandomNumber();
    TodoApp.addTodoToList(value,false,randomId);
    TodoApp.todoInput.val("");
    var newTodo = {
        userId: TodoApp.userId,
        id: randomId,
        title: value,
        completed: false
    }
    $.ajax({
      type: "POST",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      url: "/todo/",
      data: JSON.stringify(newTodo),
      success: function(data) {
        console.log("post response");
        console.log(data);
      }
    });
  },

  addTodoToList: function(value,completed,id) {
    console.log("addTodoToList");
    var newTodoTag = TodoApp.addTodoTag(value,completed,id);
    TodoApp.todoList.append(newTodoTag);
    if(TodoApp.todoList.children().length == 1) {
      TodoApp.todoList.prepend(TodoApp.addCheckAllTag);
    }
    TodoApp.todoList.slideDown();
  },

  checkComplete: function() {
    console.log("checkComplete");
    //console.log(this);
    TodoApp.checkCompleteOnElement($(this));
    
    var $item = $(this).parent();
    var description = $item.find('div.todo-description').text();
    var id = $item.find('div.todo-hidden').text();
    var completed = $item.find('input.check-todo').is(':checked');
    
    var updatedTodo = {
        userId: TodoApp.userId,
        id: id,
        title: description,
        completed: completed
    }
    $.ajax({
      type: "PUT",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      url: "/todo/"+id,
      data: JSON.stringify(updatedTodo),
      success: function(data) {
        //console.log("put response");
        //console.log(data);
      }
    });
  },

  checkCompleteOnElement: function($element) {
    //console.log("checkCompleteOnElement");
    //console.log($element);
    var todoDescription = $element.siblings(".todo-description");
    if($element.is(":checked")) {
      todoDescription.css("text-decoration","line-through");
    }
    else {
      todoDescription.css("text-decoration","");
    }
  },

  removeItem: function() {
    console.log("removeItem");
    var item = $(this).parent();
    var idToRemove = item.find('div.todo-hidden').text();
    item.remove();
    if(TodoApp.todoList.children().length == 1 && TodoApp.todoList.find('#todo-check-all')) {
      TodoApp.todoList.slideToggle();
    }
    $.ajax({
      type: "DELETE",
      //dataType: "json",
      url: "/todo/"+idToRemove,
      //data: data,
      success: function(data) {
        console.log("delete response");
        console.log(data);
      }
    });
  },

  removeAll: function() {
    console.log("removeAll");
    this.todoList.empty();
  },

  checkAll: function() {
    console.log("checkAll");
    $(this).closest('#todo-list').find('.check-todo').prop('checked', this.checked);
    $(this).closest('#todo-list').find('.check-todo').trigger('change');
  },

  ////

  toObject: function() {
    console.log("toObject");

    var jsonArray = [];

    $( "div.todo-item" ).each(function( index ) {

      var description = $(this).find('div.todo-description').text();
      var id = $(this).find('div.todo-hidden').text();
      var completed = $(this).find('input.check-todo').is(':checked');
      //console.log( index + " :  " + id + " " + description + " " + completed );

      var todoItem = {
        id: parseInt(id),
        description: description,
        completed: completed
      };

      jsonArray.push(todoItem);
    });

    return jsonArray;
  },

  fromObject: function(jsonArray) {
    console.log("fromObject");

    var arrayLength = jsonArray.length;
    for (var i = 0; i < arrayLength; i++) {
      //console.log(jsonArray[i]);
      TodoApp.addTodoToList(jsonArray[i].title,jsonArray[i].completed,jsonArray[i].id);
    }
  },

  archiving: function() {
    console.log("archive");
    
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "/todo/archive/"+TodoApp.userId,
      //data: data,
      success: function(data) {
        console.log("success archiving");
        console.log(data);
        TodoApp.getTodos();
      }
    });
  },

  ////

  addTodoTag: function(value,completed,id) {
    console.log("addTodoTag: "+value+", "+completed);

    var checked = '';
    if (true===completed) {
      checked = ' checked';
    }
    var item = $("<div class='todo-item'></div>");

    var inputTag = $("<input type ='checkbox' class='check-todo'"+checked+">");
    item.append(inputTag);

    var action = $("<div class='todo-description'></div>");
    action.text(value);
    item.append(action);

    var idTag = $("<div class='todo-hidden'></div>");
    idTag.text(id);
    item.append(idTag);

    item.append($("<div class='todo-remove' title='remove "+id+"'>X</div>"));

    if (true===completed) {
      //console.log(inputTag[0]);
      TodoApp.checkCompleteOnElement(inputTag);
    }

    return item;
  },

  addCheckAllTag: function() {
    var checkAllTag = $("<input type ='checkbox' id='todo-check-all'> complete all</input>");
    return checkAllTag;
  },

  //// [ { id: 0, userId: 1, title:'learn angular', completed:false } ] .

  getRandomNumber: function() {
    var timeInMs = Date.now();
    var randomNum = (timeInMs % 1000000000);
    return randomNum;
  }

};

$(function() { TodoApp.init(); } );
