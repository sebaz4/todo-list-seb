package com.rsa.todo.config;

import com.rsa.todo.Application;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = Application.class)
public class ApplicationConfig extends WebMvcAutoConfiguration {

    /**
     * Configure a {@link org.springframework.web.client.RestTemplate} to use Apache HttpComponents
     * {@link org.apache.http.client.HttpClient} instead of the java.net packages.
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }

}
