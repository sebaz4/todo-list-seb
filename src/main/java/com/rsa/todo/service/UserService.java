/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsa.todo.service;

import com.rsa.todo.domain.User;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.apache.log4j.Logger;

/**
 *
 * @author sebastianmac
 */
@Service
public class UserService {

    protected static final Logger log = Logger.getLogger(UserService.class);
    
    protected URL url;
    protected ObjectMapper mapper;
    protected List<User> userList;
    
    @Autowired
    private Environment env;
    
    public UserService() {
	mapper = new ObjectMapper();
	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        userList = new ArrayList<User>();
    }
    
    public String hello(String name) {
        log.debug("hello "+name);
        log.debug("url="+env.getProperty("baseUrl"));
        log.debug("size="+userList.size());
        
        String hello = "hello service user "+name;
        return hello;
    }
    
    public String getOriginAll() {
        log.debug("getOriginAll");
		
        try {
            
                String urlName = env.getProperty("baseUrl")+"users";
                log.debug("url="+urlName);
                
                url = new URL(urlName);
                
                URLConnection connection = url.openConnection();
                
                User[] users = mapper.readValue(connection.getInputStream(), User[].class);

                userList = Arrays.asList(users);
                
                for (User user : userList) {
                    log.debug("User: "+user.getName()+" - "+user.getUsername()+" - "+user.getId());
                }
                
                log.debug("size="+userList.size());

        } catch (MalformedURLException e) {
                e.printStackTrace();
        } catch (JsonGenerationException e) {
                e.printStackTrace();
        } catch (JsonMappingException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }
                
        return "done";
    }
    
    public List<User> getAll() {
        log.debug("getAll");
        
        return userList;
    }
    
    public User getOne(int id) {
        log.debug("getOne");
        
        for (User user : userList) {
            
            if (user.getId() == id) {
                return user;
            }
        }
        
        return null;
    }

}
