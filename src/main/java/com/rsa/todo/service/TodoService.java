/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsa.todo.service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsa.todo.domain.Todo;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author sebastianmac
 */
@Service
public class TodoService {
    
    protected static final Logger log = Logger.getLogger(TodoService.class);
    
    protected URL url;
    protected ObjectMapper mapper;
    protected List<Todo> todoList;
    
    @Autowired
    private Environment env;
    
    public TodoService() {
        mapper = new ObjectMapper();
	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        todoList = new ArrayList<Todo>();
    }
    
    public String hello(String name) {
        log.debug("hello "+name);
        log.debug("url="+env.getProperty("baseUrl"));
        log.debug("size="+todoList.size());
        
        String hello = "hello service todo "+name;
        return hello;
    }
    
    public String getOriginAll() {
        log.debug("getOriginAll");
		
        try {
            
                String urlName = env.getProperty("baseUrl")+"todos";
                log.debug("url="+urlName);
                
                url = new URL(urlName);
                
                URLConnection connection = url.openConnection();
                
                Todo[] todos = mapper.readValue(connection.getInputStream(), Todo[].class);

                //todoList = Arrays.asList(todos);
                
                //todoList = Collections.synchronizedList( Arrays.asList(todos) );
                
                todoList = new LinkedList<Todo>( Arrays.asList(todos) );
                
                for (Todo todo : todoList) {
                    log.debug("Todo: "+todo.getTitle()+" - "+todo.getId()+" - "+todo.getCompleted()+" - "+todo.getUserId());
                }
                
                log.debug("size="+todoList.size());

        } catch (MalformedURLException e) {
                e.printStackTrace();
        } catch (JsonGenerationException e) {
                e.printStackTrace();
        } catch (JsonMappingException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }
                
        return "done";
    }
    
    public List<Todo> getAll() {
        log.debug("getAll");
        
        return todoList;
    }
    
    public Todo getOne(int id) {
        log.debug("getOne "+id);
        
        return find(id);
    }
    
    public boolean deleteOne(int id) {

        int index = findIndex(id);
        log.debug("deleteOne "+id+" in index "+index);
        
        if (index >=0) {
            todoList.remove(index);
            return true;
        }
        
        return false;
    }
    
    public boolean updateOne(Todo todo) {

        int index = findIndex(todo.getId());
        log.debug("updateOne in index "+index);
        
        if (index >=0) {
            todoList.set(index, todo);
            return true;
        }
        
        return false;
    }
    
    public boolean createOne(Todo todo) {
        log.debug("createOne");
        
        todoList.add(todo);
        
        return true;
    }
    
    public List<Todo> getFromUserId(int id) {
        log.debug("getFromUserId "+id);
        
        List<Todo> tempList = new ArrayList<Todo>();
        
        for (Todo todo : todoList) {
            
            if (todo.getUserId() == id) {
                tempList.add(todo);
            }
        }
        
        return tempList;
    }
    
    public int archiveUserId(int id) {
        log.debug("archiveUserId "+id);
        
        int howManyArchived = 0;
        
        for (Iterator<Todo> iterator = todoList.iterator(); iterator.hasNext(); ) {
            Todo todo = iterator.next();
            if(todo.getUserId() == id && todo.getCompleted() == true) {
                iterator.remove();
                howManyArchived++;
            }
        }

        return howManyArchived;
    }
    
    public int archiveAll() {
        log.debug("archiveAll");
        
        int howManyArchived = 0;
        
        for (Iterator<Todo> iterator = todoList.iterator(); iterator.hasNext(); ) {
            Todo todo = iterator.next();
            if(todo.getCompleted() == true) {
                iterator.remove();
                howManyArchived++;
            }
        }

        return howManyArchived;
    }

    public int findIndex(int id) {
        log.debug("findIndex "+id);
        
        int index = 0;
        
        for (Todo todo : todoList) {
            
            if (todo.getId() == id) {
                log.debug("found in index "+index);
                return index;
            }
            index++;
        }
        
        return -1;
    }
    
    public Todo find(int id) {
        log.debug("find "+id);
        
        for (Todo todo : todoList) {
            
            if (todo.getId() == id) {
                log.debug("found");
                return todo;
            }
        }
        
        return null;
    }
    
}
