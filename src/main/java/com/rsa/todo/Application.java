package com.rsa.todo;

import com.rsa.todo.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;

/**
 * The initial launch point for the application.
 */
public class Application {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ApplicationConfig.class);
        application.setShowBanner(false);
        application.run(args);
    }
}
