/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsa.todo.controller;

import com.rsa.todo.domain.Todo;
import com.rsa.todo.service.TodoService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sebastianmac
 */
@RestController
@RequestMapping("/todo")
public class TodoController {
    
    protected static final Logger log = Logger.getLogger(TodoController.class);
    
    private final TodoService todoService;
    
    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
        todoService.getOriginAll();
    }
    
    @RequestMapping(value="/hello/{name}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String hello(@PathVariable String name, HttpServletResponse response) {
        log.debug("GET /hello");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.hello(name);
    }
    
    @RequestMapping(value="/origin", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getOriginAll(HttpServletResponse response) {
        log.debug("GET /origin/all");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.getOriginAll();
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Todo> getAll(HttpServletResponse response) {
        log.debug("GET /all");
        log.debug("GETtin everything ...");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.getAll();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Todo getOne(@PathVariable int id, HttpServletResponse response) {
        log.debug("GET /"+id);
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.getOne(id);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ResponseBody
    public boolean deleteOne(@PathVariable int id, HttpServletResponse response) {
        log.debug("DELETE /"+id);
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
        return todoService.deleteOne(id);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT, consumes={"application/json", "application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public boolean updateOne(@PathVariable int id, @RequestBody Todo todo, HttpServletResponse response) {
        log.debug("PUT /"+id);
        log.debug(todo.toString());
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
        return todoService.updateOne(todo);
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST, consumes={"application/json", "application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public boolean createOne(@RequestBody Todo todo, HttpServletResponse response) {
        log.debug("POST /");
        log.debug(todo.toString());
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
        return todoService.createOne(todo);
    }
    
    @RequestMapping(value="/user/{id}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Todo> getFromUserId(@PathVariable int id, HttpServletResponse response) {
        log.debug("GET /user/"+id);
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.getFromUserId(id);
    }
    
    @RequestMapping(value="/archive/{id}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public int archiveUserId(@PathVariable int id, HttpServletResponse response) {
        log.debug("GET /archive/"+id);
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.archiveUserId(id);
    }
    
    @RequestMapping(value="/archive", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public int archiveAll(HttpServletResponse response) {
        log.debug("GET /archive");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return todoService.archiveAll();
    }

}
