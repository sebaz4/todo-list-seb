/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsa.todo.controller;

import com.rsa.todo.domain.User;
import com.rsa.todo.service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sebastianmac
 */
@RestController
@RequestMapping("/user")
public class UserController {
    
    protected static final Logger log = Logger.getLogger(UserController.class);
    
    private final UserService userService;
    
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
        userService.getOriginAll();
    }
    
    @RequestMapping(value="/hello/{name}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String hello(@PathVariable String name, HttpServletResponse response) {
        log.debug("GET /hello");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return userService.hello(name);
    }
    
    @RequestMapping(value="/origin", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getOriginAll(HttpServletResponse response) {
        log.debug("GET /origin/all");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return userService.getOriginAll();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public User getOne(@PathVariable int id, HttpServletResponse response) {
        log.debug("GET /"+id);
        response.setHeader("Access-Control-Allow-Origin", "*");
        return userService.getOne(id);
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<User> getAll(HttpServletResponse response) {
        log.debug("GET /all");
        response.setHeader("Access-Control-Allow-Origin", "*");
        return userService.getAll();
    }
    
}
