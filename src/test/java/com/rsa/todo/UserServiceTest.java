/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rsa.todo;

import com.rsa.todo.service.UserService;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author sebastianmac
 */
public class UserServiceTest {
    
//    private final UserService userService;
    
//    @Autowired
//    public UserServiceTest(UserService userService) {
//        this.userService = userService;
//    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
    }

    @Test
    public void testHello() {
        System.out.println("hola test");
        //String hola = userService.hello("joe");
        String hola = "hello service user joe";
        assertEquals("must be the same",hola,"hello service user joe");
    }
    
}
